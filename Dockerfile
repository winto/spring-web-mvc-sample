FROM tomcat:8 
RUN rm -rf /usr/local/tomcat/webapps/ROOT/* 
COPY /WebContent /usr/local/tomcat/webapps/ROOT 
COPY /build /usr/local/tomcat/webapps/ROOT/WEB-INF 